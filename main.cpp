#include <Arduino.h>
#include <ModbusMaster.h>


// instantiate ModbusMaster object
ModbusMaster node;

#define FORWARD 5
#define BACKWARD 6
#define LED 13

void Motor_Enable_Disable( int direcction );

void setup() {
  // use Serial (port 0); initialize Modbus communication baud rate
  Serial.begin(9600);

  // communicate with Modbus slave ID 2 over Serial (port 0)
  node.begin(1, Serial);

  pinMode(FORWARD, INPUT);
  pinMode(BACKWARD, INPUT);
  pinMode(LED, OUTPUT);
}

void loop() {

  // slave: write TX buffer to (2) 16-bit registers starting at register 0
  digitalWrite(LED, LOW);
  // Set Controlled mode to 3 
  node.writeSingleRegister(0x00, 3);
  // Set forward speed to 250RPM
  node.writeSingleRegister(0x03, 2500);
  // Set backward speed to 250RPM
  node.writeSingleRegister(0x06, 2500);

  //Motor Enable Forward
  Motor_Enable_Disable(FORWARD);
  delay(2000);                    //2 seconds delay
  Motor_Enable_Disable(FORWARD);
  delay(2000);                    //2 seconds delay

  //Motor Enable  Backwards
  Motor_Enable_Disable(BACKWARD);
  delay(2000);                    //2 seconds delay
  Motor_Enable_Disable(BACKWARD);
  delay(2000);                    //2 seconds delay

  while(1);
}

void Motor_Enable_Disable( int direcction ){
  pinMode(direcction, OUTPUT);              //Set GPIO as output
  digitalWrite(direcction, HIGH);           //Set GPIO High level 
  delay(100);
  digitalWrite(direcction, LOW);            //Set GPIO Low Level to create a imitation of switch
  delay(100);
  pinMode(direcction, INPUT);               //Set GPIO INPUT high impedance
}
